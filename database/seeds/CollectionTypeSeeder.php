<?php

use Illuminate\Database\Seeder;

class CollectionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('collection_types')->truncate();
        DB::table('collection_types')->insert([
            [
                "name" => "Novel",
                "code" => "NV"
            ],
            [
                "name" => "Fiksi Ilmiah",
                "code" => "FI"
            ],
            [
                "name" => "Ensiklopedia",
                "code" => "EP"
            ],
            [
                "name" => "Pendidikan",
                "code" => "PD"
            ],
            [
                "name" => "Science",
                "code" => "SCI"
            ],
            [
                "name" => "Social",
                "code" => "SC"
            ],
            [
                "name" => "Budaya",
                "code" => "BD"
            ],
            [
                "name" => "Teknologi",
                "code" => "TEC"
            ],
            [
                "name" => "Agama",
                "code" => "AG"
            ],
            [
                "name" => "Ekonomi",
                "code" => "EK"
            ],
        ]);
//        factory(\App\Models\CollectionType::class,10)->create();
    }
}
