<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        \Illuminate\Database\Eloquent\Model::unguard();

        $this->call(AdminSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(AdminRole::class);
        $this->call(MenuRole::class);

        $this->call(MediaTypeSeeder::class);
        $this->call(CollectionTypeSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(ItemStatusSeeder::class);
        $this->call(CollectionSeeder::class);
        $this->call(LoanStatusSeeder::class);
        $this->call(UserSeeder::class);

        \Illuminate\Database\Eloquent\Model::reguard();
        DB::statement("SET FOREIGN_KEY_CHECKS = 1");
        // $this->call(UsersTableSeeder::class);
    }
}
