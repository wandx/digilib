<?php

use Illuminate\Database\Seeder;

class LoanStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loan_statuses')->truncate();
        DB::table('loan_statuses')->insert([
            [
                'status' => 'dipinjam',
                'code' => 'loaned'
            ],
            [
                'status' => 'dikembalikan',
                'code' => 'returned'
            ],
            [
                'status' => 'rusak',
                'code' => 'broken'
            ],
            [
                'status' => 'hilang',
                'code' => 'lost'
            ]
        ]);
    }
}
