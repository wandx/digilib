<?php

use Illuminate\Database\Seeder;

class MediaTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('media_types')->truncate();
        DB::table("media_types")->insert([
            [
                "name" => "Book",
                "slug" => "book"
            ],
            [
                "name"=>"Audio",
                "slug"=>"audio"
            ],
            [
                "name" => "Video",
                "slug" => "video"
            ],
            [
                "name" => "Ebook",
                "slug" => "ebook"
            ]

        ]);
//        factory(\App\Models\MediaType::class,10)->create();
    }
}
