<?php

use Illuminate\Database\Seeder;

class ItemStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_statuses')->truncate();
        DB::table('item_statuses')->insert([
            [
                'status' => 'tersedia',
                'code' => 'available'
            ],
            [
                'status' => 'tidak tersedia',
                'code' => 'na'
            ],
            [
                'status' => 'tersedia tapi tidak dipinjamkan',
                'code' => 'not_for_loan'
            ],
            [
                'status' => 'disimpan staff',
                'code' => 'keep_by_staff'
            ],
            [
                'status' => 'hilang',
                'code' => 'lost'
            ],
            [
                'status' => 'rusak',
                'code' => 'broken'
            ]
        ]);
    }
}
