<?php

use Illuminate\Database\Seeder;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('collections')->truncate();
        DB::table('items')->truncate();

        factory(\App\Models\Collection::class,10)->create()->each(function($model){
            for($i=0;$i<3;$i++){
                $model->items()->create([
                    'code' => rand(1000000,9999999),
                    'location_id' => rand(1,10),
                    'status_id' => 1,
                    'no_rak' => rand(1,10).':'.rand(1,100).','.rand(1,1000),
                    'media_type_id' => rand(1,4)
                ]);
            }
        });
    }
}
