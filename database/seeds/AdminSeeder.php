<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->truncate();
        DB::table('admins')->insert([
            'name' => 'SuperUser',
            'username' => 'superuser',
            'email' => 'superuser@gmail.com',
            'password' => bcrypt('superuser'),
            'last_login' => \Carbon\Carbon::now(),
            'avatar' => 'http://placehold.it/300x300',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
