<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->truncate();
        DB::table('sub_menus')->truncate();
        DB::table('sub_sub_menus')->truncate();

        DB::table('menus')->insert([
            [
                'name' => 'Dashboard',
                'slug' => 'dashboard',
                'icon' => 'fa fa-dashboard'
            ],
            [
                'name' => 'User Manager',
                'slug' => 'usermanager',
                'icon' => 'fa fa-users'
            ],
            [
                'name' => 'Inventaris',
                'slug' => 'collection',
                'icon' => 'fa fa-building'
            ],
            [
                'name' => 'Sirkulasi',
                'slug' => 'sirkulasi',
                'icon' => 'fa fa-exchange'
            ],
            [
                'name' => 'Siswa',
                'slug' => 'siswa',
                'icon' => 'fa fa-address-card'
            ]
        ]);

        DB::table('sub_menus')->insert([
            [
                'name' => 'Lists',
                'slug' => 'lists',
                'icon' => 'fa fa-list-ol',
                'menu_id' => 2
            ],
            [
                'name' => 'Roles',
                'slug' => 'roles',
                'icon' => 'fa fa-list-ol',
                'menu_id' => 2
            ],
            [
                'name' => 'Tambah',
                'slug' => 'add',
                'icon' => 'fa fa-plus',
                'menu_id' => 3
            ],
            [
                'name' => 'Lists',
                'slug' => 'lists',
                'icon' => 'fa fa-list-ol',
                'menu_id' => 3
            ],
            [
                'name' => 'Lists',
                'slug' => 'lists',
                'icon' => 'fa fa-list-ol',
                'menu_id' => 4
            ],
            [
                'name' => 'Tambah',
                'slug' => 'add',
                'icon' => 'fa fa-plus',
                'menu_id' => 5
            ],
            [
                'name' => 'Lists',
                'slug' => 'lists',
                'icon' => 'fa fa-list-ol',
                'menu_id' => 5
            ],
        ]);
    }
}
