<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'id' => $faker->uuid,
        'name' => $faker->name,
        'nim' => rand(11111111,99999999),
        'address' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


// media type
$factory->define(\App\Models\MediaType::class,function(\Faker\Generator $faker){
    $name = $faker->words(2,true);
    return [
        'name' => $name,
        'slug' => \Illuminate\Support\Str::slug($name)
    ];
});

// location
$factory->define(\App\Models\Location::class,function(\Faker\Generator $faker){
    return [
        'name' => 'Gedung '.$faker->name,
        'address' => $faker->address,
    ];
});

// collection type
$factory->define(\App\Models\CollectionType::class,function(\Faker\Generator $faker){
    return [
        'name' => $faker->words(2,true),
        'code' => strtoupper(str_random(2))
    ];
});

// collection
$factory->define(\App\Models\Collection::class,function(\Faker\Generator $faker){
    return [
        'id' => $faker->uuid,
        'title' => $faker->words(3,true),
        'subtitle' => $faker->words(6,true),
        'collection_type_id' => rand(1,10),
        'code' => rand(1000000,9999999),
        'type' => $faker->randomElement(['issn','isbn']),
        'language' => $faker->randomElement(['indonesia','english']),
        'pub' => $faker->company,
        'pub_year' => $faker->year,
        'pub_city' => $faker->randomElement(['yogyakarta','jakarta','surabaya','bandung']),
        'desc' => rand(1,1000).' '.$faker->words(2,true),
        'authors' => $faker->name,
        'subject' => $faker->words(3,true),
    ];
});