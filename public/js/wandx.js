$('[data-toggle="tooltip"]').tooltip();

$('.trigger-advanced').click(function(){
    $(this).prev('.advanced').toggleClass('show-me');
    // dis();
});

// typeahead for issn
$('#title').typeahead({
    hint: true,
    highlight: true,
    minLength: 4}, {
    name: 'states',
    source: function(query,sy,asy){
        return $.getJSON('/sTitle', { query: query }, function (data) {
            return asy(data);
        });
    }
});

// typeahead for issn
$('#issn').typeahead({
    hint: true,
    highlight: true,
    minLength: 4}, {
    name: 'states',
    source: function(query,sy,asy){
        return $.getJSON('/sIssn', { query: query }, function (data) {
            return asy(data);
        });
    }
});

// typeahead for subject
$('#subject').typeahead({
    hint: true,
    highlight: true,
    minLength: 4}, {
    name: 'states',
    source: function(query,sy,asy){
        return $.getJSON('/sSubject', { query: query }, function (data) {
            return asy(data);
        });
    }
});

// typeahead for author
$('#author').typeahead({
    hint: true,
    highlight: true,
    minLength: 4}, {
    name: 'states',
    source: function(query,sy,asy){
        return $.getJSON('/sAuthor', { query: query }, function (data) {
            return asy(data);
        });
    }
});

function dis() {
    var e = $('.advanced');

    e.find('input').prop('disabled',true);
    e.find('select').prop('disabled',true);
}