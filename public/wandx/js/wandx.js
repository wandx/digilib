// show password
function showPassword(element){
    var attr = $(element).attr('type');

    if(attr === "text"){
        $(element).attr('type','password');
    }else{
        $(element).attr('type','text');
    }
}

// readfile
function readFile(input,elem) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(elem).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
        $(elem).removeClass('hidden');
        $(elem).prev().addClass('hidden');
    }
}