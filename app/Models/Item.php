<?php

namespace App\Models;

use App\Models\Pivot\LoanPivot;
use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use UuidForKey;

    protected $guarded = ['id'];
    public $incrementing = false;
    protected $casts = ['loans.id'=>'string'];

    public function collection(){
        return $this->belongsTo(Collection::class);
    }

    public function location(){
        return $this->belongsTo(Location::class);
    }

    public function item_status(){
        return $this->belongsTo(ItemStatus::class,'status_id');
    }

    public function media_type(){
        return $this->belongsTo(MediaType::class);
    }

    public function file(){
        return $this->hasOne(File::class);
    }

    public function loans(){
        return $this->belongsToMany(User::class,'loans','item_id','user_id')->using(LoanPivot::class)->wherePivot('loan_status_id',1)->withPivot([
            'loan_start','loan_end','loan_status_id','admin_id','id'
        ]);
    }
}
