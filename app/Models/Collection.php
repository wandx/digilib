<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use UuidForKey;

    public $incrementing = false;
    protected $guarded = ['id'];
    protected $appends = [
        'jml_item','available_item','loaned_item','on_staff'
    ];

    public function collection_type(){
        return $this->belongsTo(CollectionType::class);
    }

    public function items(){
        return $this->hasMany(Item::class);
    }

    // hitung semua item
    public function getJmlItemAttribute(){
        return (new Item())->newQuery()->where('collection_id',$this->id)->count();
    }

    // hitung item yang tersedia
    public function getAvailableItemAttribute(){
//        return $this->newQuery()->with(['items'=>function($item){
//            $item->wherehas('item_status',function ($is){
//                $is->where('code','available');
//            });
//        }])->first()->items->count() ?? 0;
        return (new Item())->newQuery()->whereHas('item_status',function($model){
            $model->where('code','available');
        })->where('collection_id',$this->id)->count();
    }

    // hitung item yang dipinjam
    public function getLoanedItemAttribute(){
//        return $this->newQuery()->with(['items'=>function($item){
//                $item->wherehas('item_status',function ($is){
//                    $is->where('code','na');
//                });
//            }])->first()->items->count() ?? 0;

        return (new Item())->newQuery()->whereHas('item_status',function($model){
            $model->where('code','na');
        })->where('collection_id',$this->id)->count();
    }

    public function getOnStaffAttribute(){
        return (new Item())->newQuery()->whereHas('item_status',function($model){
            $model->where('code','keep_by_staff');
        })->where('collection_id',$this->id)->count();
    }
}
