<?php namespace App\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class LoanPivot extends Pivot{
    protected $casts = ['id' => 'string'];
}