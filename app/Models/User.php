<?php

namespace App\Models;

use App\Models\Pivot\LoanPivot;
use App\Traits\UuidForKey;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,UuidForKey;
    public $incrementing = false;
    protected $appends = [
        'active_loan','done_loan','broken_loan','lost_loan'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id"];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function loans(){
        return $this->belongsToMany(Item::class,'loans','user_id','item_id')->using(LoanPivot::class)->wherePivot('loan_status_id',1)->withPivot([
            'loan_start','loan_end','loan_status_id','admin_id','id'
        ]);
    }

    public function returned(){
        return $this->belongsToMany(Item::class,'loans','user_id','item_id')->using(LoanPivot::class)->wherePivot('loan_status_id',2);
    }

    public function getActiveLoanAttribute(){
        $id = $this->id;
        return (new Loan())->whereHas('user',function($model) use ($id){
            $model->where('id',$id);
        })->where('loan_status_id',1)->count();
    }

    public function getDoneLoanAttribute(){
        $id = $this->id;
        return (new Loan())->whereHas('user',function($model) use ($id){
            $model->where('id',$id);
        })->where('loan_status_id',2)->count();
    }

    public function getBrokenLoanAttribute(){
        $id = $this->id;
        return (new Loan())->whereHas('user',function($model) use ($id){
            $model->where('id',$id);
        })->where('loan_status_id',3)->count();
    }

    public function getLostLoanAttribute(){
        $id = $this->id;
        return (new Loan())->whereHas('user',function($model) use ($id){
            $model->where('id',$id);
        })->where('loan_status_id',4)->count();
    }

    public function getLateLoanAttribute(){
        $id = $this->id;
        $now = Carbon::now()->format('Y-m-d H:i:s');

        return (new Loan())->whereHas('user',function($model) use ($id,$now){
            $model->where('id',$id);
        })->where('loan_status_id',1)->where('loan_end','<',$now)->count();
    }
}
