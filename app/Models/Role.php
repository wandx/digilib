<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    public function admins(){
        return $this->belongsToMany(Admin::class);
    }

    public function menus(){
        return $this->belongsToMany(Menu::class);
    }
}
