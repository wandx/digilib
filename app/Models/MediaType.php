<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaType extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function collections(){
        return $this->hasMany(Collection::class);
    }
}
