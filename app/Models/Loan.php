<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use UuidForKey;

    protected $guarded = ['id'];
    public $incrementing = false;


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function item(){
        return $this->belongsTo(Item::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function getIsLateAttribute(){
        $end = Carbon::parse($this->loan_end);
        $now = Carbon::now();

        return $end->greaterThan($now) ? true:false;
    }
}
