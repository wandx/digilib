<?php

// Build menu
function menuData(){
    $menu = new \App\Models\Menu();
    $menu = $menu->newQuery()->whereHas('roles',function($role){
        $role->where('id',session('role_id'));
    })->with('sub_menus.sub_sub_menus')->get();

    return $menu;
}

// get all menu
function allMenuData(){
    $menu = new \App\Models\Menu();
    $menu = $menu->newQuery()->with('sub_menus.sub_sub_menus')->get();

    return $menu;
}

// Get role by id
function roleName($id){
    $role = new \App\Models\Role();
    $role = $role->newQuery()->find($id);

    return $role == null ? "-" : $role->name;
}

// Get last activity (admin)
function lastActivity(){
    if(auth('admin')->check()){
        $ac = new \App\Models\AdminActivity();
        $ac = $ac->newQuery()->whereHas('admin',function($admin){
            $admin->where('id',auth('admin')->user()->id);
        })->orderBy('time','desc')->first();

        return $ac == null ? "no activity yet":$ac->time->format('d M Y, H:i:s');
    }
    return "no activity";
}

function roleData(){
    $role = new \App\Models\Role();
    return $role->newQuery()->pluck('name','id');
}

function getLoanStatus($id){
    return (new \App\Models\LoanStatus())->newQuery()->find($id)->status;
}

function cekTerlambat($id){
    $x = (new \App\Models\Loan())->newQuery()->find($id);

    $end = \Carbon\Carbon::parse($x->loan_end);
    $now = \Carbon\Carbon::now();

    if($end->greaterThan($now)){
        return true;
    }

    return false;
}

function getNimById($nim){
    return (new \App\Models\User())->newQuery()->where('nim',$nim)->first()->id ?? null;
}

function getMedia(){
    return (new \App\Models\MediaType())->newQuery()->pluck('name','id')->toArray();
}

function getCollectionType(){
    return (new \App\Models\CollectionType())->newQuery()->pluck('name','id')->toArray();
}

function getLocation(){
    return (new \App\Models\Location())->newQuery()->pluck('name','id')->toArray();
}