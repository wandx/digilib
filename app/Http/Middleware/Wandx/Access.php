<?php

namespace App\Http\Middleware\Wandx;

use App\Models\Menu;
use Closure;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$menu)
    {
        // init access
        $hasAccess = false;

        // check session exist
        $cek = session()->has('role_id');

        if($cek){
            $menuData = new Menu();

            // is role has access to menu?
            $cekMenu = $menuData->newQuery()->whereHas('roles',function($role) use ($menu){
                $role->where('id',session('role_id'));
            })->where('slug',$menu)->first();

            // if yes set access = true
            if($cekMenu != null){
                $hasAccess = true;
            }
        }

        return $hasAccess ? $next($request) : abort('403','noaccess');
    }
}
