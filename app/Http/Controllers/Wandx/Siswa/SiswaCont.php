<?php

namespace App\Http\Controllers\Wandx\Siswa;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiswaCont extends Controller
{
    public function index(Request $request,User $user){
        $data = [
            'lists' => $this->userQuery($request,$user)
        ];

        return view('wandx.siswa.index',$data);
    }

    public function add(){
        return view("wandx.siswa.add");
    }

    public function edit($id,User $user){
        $data = [
            "user" => $user->newQuery()->find($id)
        ];

        return view("wandx.siswa.edit",$data);
    }

    public function store(Request $request,User $user){
        $data = [
            "nim" =>$request->input("nim"),
            "name" => $request->input("name"),
            "address" => $request->input("address"),
            "email" => $request->input("email"),
            "password" => bcrypt("siswa")
        ];

        $user->newQuery()->create($data);
        return redirect()->route("adm.siswa");
    }

    public function update($id,Request $request,User $user){
        $data = [
            "nim" =>$request->input("nim"),
            "name" => $request->input("name"),
            "address" => $request->input("address"),
            "email" => $request->input("email"),
            "password" => bcrypt("siswa")
        ];

        $user->newQuery()->find($id)->update($data);
        return redirect()->back();
    }

    public function destroy($id,User $user){
        $user->newQuery()->find($id)->delete();
        return redirect()->back();
    }

    private function userQuery(Request $request,User $user){
        $user = $user->newQuery();

        return $user->latest()->paginate(10);
    }
}
