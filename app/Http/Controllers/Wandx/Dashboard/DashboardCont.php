<?php

namespace App\Http\Controllers\Wandx\Dashboard;

use App\Models\Collection;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardCont extends Controller
{
    public function index(){
        $data = [
            "collection" => (new Collection())->count(),
            "user" => (new User())->count()
        ];
        return view('wandx.dashboard.index',$data);
    }
}
