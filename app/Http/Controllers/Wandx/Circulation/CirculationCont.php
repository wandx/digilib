<?php

namespace App\Http\Controllers\Wandx\Circulation;

use App\Models\Collection;
use App\Models\Item;
use App\Models\ItemStatus;
use App\Models\Loan;
use App\Models\Location;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CirculationCont extends Controller
{
    public function index(Request $request,Item $item,ItemStatus $status,Location $location){
        $data = [
            'lists' => $this->itemQuery($request,$item),
            'status' => $status->newQuery()->pluck('status','id')->toArray(),
            'lokasi' => $location->newQuery()->pluck('name','id')->toArray(),
        ];

        return view('wandx.sirkulasi.index',$data);
    }

    public function detail($id,Item $item){
        $data = [
            'item' => $item->newQuery()->find($id)
        ];
        return view('wandx.sirkulasi.detail',$data);
    }

    public function pinjam(Request $request,Loan $loan,Item $item){
        $userid = getNimById($request->input('nis'));

        if($userid == null){
            return redirect()->back()->with(['failed'=>'NIS tidak ditemukan']);
        }
        $data = [
            'loan_start' => Carbon::parse($request->input('in'))->format('Y-m-d H:i:s'),
            'loan_end' => Carbon::parse($request->input('ret'))->format('Y-m-d H:i:s'),
            'item_id' => $request->input('item-id'),
            'user_id' => $userid,
            'admin_id' => auth('admin')->user()->id,
            'loan_status_id' => 1
        ];

        $loan->newQuery()->create($data);
        $item->newQuery()->find($request->input('item-id'))->update([
            'status_id' => 2
        ]);

        return redirect()->back();
    }

    public function kembali(Request $request,Loan $loan,Item $item){
        $loan_id = $request->input('loan-id');
        $item_id = $request->input('item-id');

        $loan->newQuery()->find($loan_id)->update([
            'loan_status_id' => 2
        ]);

        $item->newQuery()->find($item_id)->update(['status_id'=>4]);
        return redirect()->back();
    }

    public function keRak(Request $request,Item $item){
        $item->newQuery()->find($request->input('item-id'))->update(['status_id'=>1]);
        return redirect()->back();
    }

    public function titleList(Request $request,Collection $collection){
        return $collection->newQuery()->where('title','like','%'.$request->input('query').'%')->pluck('title');
    }

    public function issnList(Request $request,Collection $collection){
        return $collection->newQuery()->where('code','like','%'.$request->input('query').'%')->pluck('code');
    }

    public function codeList(Request $request,Item $item){
        return $item->newQuery()->where('code','like','%'.$request->input('query').'%')->pluck('code');
    }

    public function pubList(Request $request,Collection $collection){
        return $collection->newQuery()->where('pub','like','%'.$request->input('query').'%')->pluck('pub');
    }

    public function subjectList(Request $request,Collection $collection){
        return $collection->newQuery()->where('subject','like','%'.$request->input('query').'%')->pluck('subject');
    }

    public function nislist(Request $request,User $user){
//        return $user->newQuery()->get();
        return $user->newQuery()->where('nim','like','%'.$request->input('query').'%')->pluck('nim');
    }

    public function getUserByNis($nis,User $user){
        $data = [
            'user' => $user->newQuery()->where('nim',$nis)->first()
        ];

        return view('wandx.sirkulasi.info_siswa',$data);
    }

    private function itemQuery(Request $request,Item $item){
        $c = $item->newQuery();

        if($request->has('code')){
            $c->where('code',$request->input('code'));
        }

        if($request->has('issn')){
            $c->whereHas('collection',function($model) use ($request){
                $model->where('code',$request->input('issn'));
            });
        }

        if($request->has('judul')){
            $c->whereHas('collection',function($model) use ($request){
                $model->where('title','like','%'.$request->input('judul').'%');
            });
        }

        if($request->has('penerbit')){
            $c->whereHas('collection',function($model) use ($request){
                $model->where('pub','like','%'.$request->input('penerbit').'%');
            });
        }

        if($request->has('lokasi')){
            $c->where('location_id',$request->input('lokasi'));
        }

        if($request->has('status')){
            $c->where('status_id',$request->input('status'));
        }

        $c->orderBy('created_at','asc');

        return $c->paginate(10);
    }
}
