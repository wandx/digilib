<?php

namespace App\Http\Controllers\Wandx\Collection;

use App\Models\Collection;
use App\Models\CollectionType;
use App\Models\File;
use App\Models\Item;
use App\Models\ItemStatus;
use App\Models\Location;
use App\Models\MediaType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Yajra\Datatables\Datatables;

class CollectionCont extends Controller
{
    public function index(Request $request,Collection $collection){
        $data = [
            'lists' => $this->collectionQuery($request,$collection)
        ];

        return view('wandx.collection.index',$data);
    }

    private function collectionQuery(Request $request,Collection $collection){
        $c = $collection->newQuery();



        if($request->has('code')){
            $c->where('code',$request->input('code'));
        }

        if($request->has('judul')){
            $c->where('title',"like","%".$request->input('judul')."%");
        }

        if($request->has('penerbit')){
            $c->where('pub',$request->input('penerbit'));
        }

        if($request->has('subyek')){
            $c->where('subject',$request->input('subyek'));
        }

//        $c->orderBy('created_at','desc');

        return $c->latest()->paginate(10);
    }

    public function add(CollectionType $collectionType){
        $data = [
            'category' => $collectionType->newQuery()->pluck('name','id'),
        ];
        return view("wandx.collection.add",$data);
    }

    public function edit($id,Collection $collection,MediaType $mediaType,CollectionType $collectionType,ItemStatus $itemStatus,Location $location){
        $data = [
            'collection' => $collection->newQuery()->find($id),
            'media' => $mediaType->newQuery()->pluck('name','id'),
            'category' => $collectionType->newQuery()->pluck('name','id'),
            'status' => $itemStatus->newQuery()->pluck('status','id'),
            'location' => $location->newQuery()->pluck('name','id')
        ];
        return view('wandx.collection.edit',$data);
    }

    public function getItem($id,Item $item,ItemStatus $status,MediaType $mediaType,Location $location){
        $data = [
            'item' => $item->newQuery()->find($id),
            'status' => $status->newQuery()->pluck('status','id'),
            'media' => $mediaType->newQuery()->pluck('name','id'),
            'location' => $location->newQuery()->pluck('name','id')
        ];

        return view('wandx.collection.modal_edit_item',$data);
    }

    public function storeItem(Request $request,Item $item){
        $data = [
            'code' => $request->input('code'),
            'status_id' => $request->input('status_id'),
            'media_type_id' => $request->input('media_type_id'),
            'collection_id' => $request->input('collection-id'),
            'location_id' => $request->input('location_id'),
            'no_rak' => $request->input('no_rak')
        ];

        $store = $item->newQuery()->create($data);

        if($request->hasFile('file')){
            $name = $request->input('code').'_'.str_random(6).'.'.$request->file('file')->extension();
            $path = storage_path('files');
            $request->file('file')->move($path,$name);
            $fileData = [
                'filename' => $name,
                'path' => $path,
                'admin_id' => auth('admin')->user()->id,
                'item_id' => $store->id
            ];
            $store->file()->create($fileData);
        }
        return redirect()->back();

    }

    public function updateItem($id,Request $request,Item $item,File $files){
        $old = $item->newQuery()->find($id)->file;
        $data = [
            'code' => $request->input('code'),
            'status_id' => $request->input('status_id'),
            'media_type_id' => $request->input('media_type_id'),
            'location_id' => $request->input('location_id'),
            'no_rak' => $request->input('no_rak')
        ];

        $item->newQuery()->find($id)->update($data);



        if($request->hasFile('file')){
            if($old != null){
                try{
                    \File::delete($old->path.$old->filename);
                }catch (\Exception $exception){}
            }
            $name = $request->input('code').'_'.str_random(6).'.'.$request->file('file')->extension();
            $path = storage_path('files');
            $request->file('file')->move($path,$name);
            $fileData = [
                'filename' => $name,
                'path' => $path,
                'admin_id' => auth('admin')->user()->id,
                'item_id' => $id
            ];
            $files->newQuery()->updateOrCreate(['item_id'=>$id],$fileData);
        }
        return redirect()->back();
    }

    public function deleteFile($id,File $file){
        $file->newQuery()->find($id)->delete();
    }

    public function delete_item($id,Item $item){
        $item->newQuery()->find($id)->delete();
        return redirect()->back();
    }

    public function store(Request $request,Collection $collection){
        $data = [
            "title" => $request->input("title"),
            "subtitle" => $request->input("subtitle"),
            "collection_type_id" => $request->input("collection_type_id"),
            "code" => $request->input("code"),
            "type" => $request->input("type"),
            "language" => $request->input("language"),
            "pub" => $request->input("pub"),
            "pub_year" => $request->input("pub_year"),
            "pub_city" => $request->input("pub_city"),
            "desc" => $request->input("desc"),
            "authors" => $request->input("authors"),
            "subject" => $request->input("subject")
        ];

        $store = $collection->create($data);
        return redirect()->route("adm.collection.edit",["id"=>$store->id]);
    }

    public function update($id,Request $request,Collection $collection){
        $data = [
            "title" => $request->input("title"),
            "subtitle" => $request->input("subtitle"),
            "collection_type_id" => $request->input("collection_type_id"),
            "code" => $request->input("code"),
            "type" => $request->input("type"),
            "language" => $request->input("language"),
            "pub" => $request->input("pub"),
            "pub_year" => $request->input("pub_year"),
            "pub_city" => $request->input("pub_city"),
            "desc" => $request->input("desc"),
            "authors" => $request->input("authors"),
            "subject" => $request->input("subject")
        ];

        $store = $collection->newQuery()->find($id)->update($data);
        return redirect()->back();
    }

    public function destroy($id,Collection $collection){
        $collection->newQuery()->find($id)->delete();
        return redirect()->back();
    }

    public function getData($id,Collection $collection){}

    public function titleList(Request $request,Collection $collection){
        return $collection->newQuery()->where('title','like','%'.$request->input('query').'%')->pluck('title');
    }

    public function issnList(Request $request,Collection $collection){
        return $collection->newQuery()->where('code','like','%'.$request->input('query').'%')->pluck('code');
    }

    public function pubList(Request $request,Collection $collection){
        return $collection->newQuery()->where('pub','like','%'.$request->input('query').'%')->pluck('pub');
    }

    public function subjectList(Request $request,Collection $collection){
        return $collection->newQuery()->where('subject','like','%'.$request->input('query').'%')->pluck('subject');
    }
}
