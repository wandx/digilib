<?php

namespace App\Http\Controllers\Client\Lists;

use App\Models\Collection;
use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListCont extends Controller
{
    public function index(Request $request,Collection $collection){
        $data = [
            'lists' => $this->listQuery($request,$collection)
        ];
        return view('client.list',$data);
    }

    public function detail($id,Collection $collection){
        $data = [
            'collection' => $collection->newQuery()->find($id)
        ];
        return view('client.detail',$data);
    }

    public function download($file_id,File $file){
        $data = $file->newQuery()->find($file_id);
        return response()->download($data->path.'/'.$data->filename);
    }

    private function listQuery(Request $request,Collection $collection){
        $collection = $collection->newQuery();

        if($request->has('title')){
            $collection->where('title','like','%'.$request->input('title').'%');
        }

        if($request->has('issn')){
            $collection->where('code',$request->input('issn'));
        }

        if($request->has('subject')){
            $collection->where('subject','like','%'.$request->input('subject').'%');
        }

        if($request->has('author')){
            $collection->where('authors','like','%'.$request->input('author').'%');
        }

        if($request->has('media')){
            $collection->whereHas('items',function($model) use ($request){
                $model->where('media_type_id',$request->input('media'));
            });
        }

        if($request->has('collection')){
            $collection->where('collection_type_id',$request->input('collection'));
        }

        if($request->has('location')){
            $collection->whereHas('items',function($model) use ($request){
                $model->where('location_id',$request->input('location_id'));
            });
        }

        return $collection->paginate(7);
    }
}
