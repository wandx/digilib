<?php

namespace App\Http\Controllers\Client\Home;

use App\Models\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeCont extends Controller
{
    public function index(){
        return view('client.index');
    }

    public function titleList(Request $request,Collection $collection){
        return $collection->newQuery()->where('title','like','%'.$request->input('query').'%')->pluck('title');
    }

    public function issnList(Request $request,Collection $collection){
        return $collection->newQuery()->where('code','like','%'.$request->input('query').'%')->pluck('code');
    }

    public function authorList(Request $request,Collection $collection){
        return $collection->newQuery()->where('authors','like','%'.$request->input('query').'%')->pluck('authors');
    }

    public function pubList(Request $request,Collection $collection){
        return $collection->newQuery()->where('pub','like','%'.$request->input('query').'%')->pluck('pub');
    }

    public function subjectList(Request $request,Collection $collection){
        return $collection->newQuery()->where('subject','like','%'.$request->input('query').'%')->pluck('subject');
    }
}
