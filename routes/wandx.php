<?php

Route::group(['namespace'=>'Auth'],function(){
    Route::get('/',['middleware'=>'guest:admin','uses'=>'LoginCont@index'])->name('adm.login');
    Route::post('login','LoginCont@doLogin')->name('adm.login.post');
    Route::get('roles',['middleware'=>'auth:admin','uses'=>'LoginCont@roles'])->name('adm.select_role');
    Route::post('roles',['middleware'=>'auth:admin','uses'=>'LoginCont@rolesPost'])->name('adm.select_role.post');
});

Route::group(['middleware'=>['auth:admin','hasmanyrole'],'as'=>'adm.'],function(){
    // Logout
    Route::get('logout','Auth\LoginCont@logout');
    // Dashboard
    Route::group(['namespace'=>'Dashboard','prefix'=>'dashboard','middleware'=>'access:dashboard'],function(){
        Route::get('/','DashboardCont@index')->name('dashboard');
    });

    // User Manager
    Route::group(['namespace'=>'Usermanager','prefix'=>'usermanager','middleware'=>'access:usermanager'],function(){
        Route::get('lists','UserCont@lists')->name('usermanager.lists');
        Route::post('store','UserCont@store')->name('usermanager.store');
        Route::get('destroy/{id}','UserCont@destroy');
        Route::post('update/{id}','UserCont@update');
        Route::get('userdata/{id}',function($id){
            return (new \App\Models\Admin())->newQuery()->where('id',$id)->with('roles')->first();
        });

        Route::group(['prefix'=>'roles'],function(){
            Route::get('/','RoleCont@index')->name('roles');
            Route::get('menu_ids/{role_id}','RoleCont@menuIds');
            Route::post('update_menu','RoleCont@updateMenu')->name('update_menu');
            Route::get('remove_access/{menu_id}/{role_id}','RoleCont@removeAccess');
        });
    });

    // Collection
    Route::group(['namespace'=>'Collection','prefix'=>'collection','middleware'=>'access:collection'],function(){
        Route::get('lists','CollectionCont@index')->name('collection');
        Route::get('add','CollectionCont@add')->name('collection.add');
        Route::post('add','CollectionCont@store')->name('collection.store');
        Route::get('edit/{id}','CollectionCont@edit')->name('collection.edit');
        Route::post('edit/{id}','CollectionCont@update')->name('collection.update');
        Route::get('get_data/{id?}','CollectionCont@getData')->name('collection.data');
        Route::get('destroy/{id}','CollectionCont@destroy')->name('collection.destroy');

        Route::get('itemdata/{item_id?}','CollectionCont@getItem')->name('collection.item');
        Route::post('store_item','CollectionCont@storeItem')->name('collection.item.store');
        Route::post('update_item/{item_id}','CollectionCont@updateItem')->name('collection.item.update');
        Route::get('delete_file/{id?}','CollectionCont@deleteFile')->name('collection.delete_file');
        Route::get("delete_item/{id}","CollectionCont@delete_item")->name("collection.delete_item");

        // typeahead
        Route::get('sTitle','CollectionCont@titleList')->name('th.title');
        Route::get('sIssn','CollectionCont@issnList')->name('th.issn');
        Route::get('sPub','CollectionCont@pubList')->name('th.pub');
        Route::get('sSubject','CollectionCont@subjectList')->name('th.subject');
    });

    // Sirkulasi
    Route::group(['namespace'=>'Circulation','prefix'=>'sirkulasi','middleware'=>'access:sirkulasi'],function(){
        Route::get('lists','CirculationCont@index')->name('sirkulasi');
        Route::get('/detail/{code}','CirculationCont@detail')->name('sirkulasi.detail');
        Route::get('siswa/{nis?}','CirculationCont@getUserByNis')->name('sirkulasi.siswa');
        Route::post('pinjamkan','CirculationCont@pinjam')->name('sirkulasi.pinjamkan');
        Route::post('kembalikan','CirculationCont@kembali')->name('sirkulasi.kembalikan');
        Route::post('kerak','CirculationCont@keRak')->name('sirkulasi.kerak');

        // typeahead
        Route::get('sTitle','CirculationCont@titleList')->name('th-cir.title');
        Route::get('sIssn','CirculationCont@issnList')->name('th-cir.issn');
        Route::get('sPub','CirculationCont@pubList')->name('th-cir.pub');
        Route::get('sCode','CirculationCont@codeList')->name('th-cir.code');
        Route::get('sNis','CirculationCont@nisList')->name('th-cir.nis');
    });

    // Siswa
    Route::group(['namespace'=>'Siswa','prefix'=>'siswa','middleware'=>'access:siswa'],function(){
        Route::get('lists','SiswaCont@index')->name('siswa');
        Route::get("add","SiswaCont@add")->name("siswa.add");
        Route::post("add","SiswaCont@store")->name("siswa.add.post");
        Route::get("edit/{id}","SiswaCont@edit")->name("siswa.edit");
        Route::post("edit/{id}","SiswaCont@update")->name("siswa.edit.post");
        Route::get("delete/{id}","SiswaCont@destroy")->name("siswa.delete");
    });

    Route::get('jal','Collection\CollectionCont@collectionData');
});