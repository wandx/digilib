<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'Client'],function(){
    Route::group(['namespace'=>'Home'],function(){
        Route::get('/','HomeCont@index')->name('home');
        Route::get('sTitle','HomeCont@titleList');
        Route::get('sIssn','HomeCont@issnList');
        Route::get('sSubject','HomeCont@subjectList');
        Route::get('sAuthor','HomeCont@authorList');
    });

    Route::group(['namespace'=>'Lists'],function(){
        Route::get('list','ListCont@index')->name('lists');
        Route::get('list/{id}','ListCont@detail')->name('detail');
        Route::get('download/{file_id}','ListCont@download')->name('download');
    });
});
