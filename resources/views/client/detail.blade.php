@extends('client.master')

@section('contents')
    @include('client.partial.navbar')
    <div class="container">
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Informasi
                    </h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>{{ strtoupper($collection->type) }}</th>
                            <td>{{ $collection->code }}</td>
                        </tr>
                        <tr>
                            <th>Judul</th>
                            <td>
                                {{ $collection->title }} {!! $collection->subtitle != null ? ' : <br>':null !!}
                                @if($collection->subtitle != null)
                                    <small>{{ $collection->subtitle }}</small>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Penerbit</th>
                            <td>{{ $collection->pub }}, {{ $collection->pub_year }}</td>
                        </tr>
                        <tr>
                            <th>Tempat terbit</th>
                            <td>{{ ucfirst($collection->pub_city) }}</td>
                        </tr>
                        <tr>
                            <th>Bahasa</th>
                            <td>{{ ucfirst($collection->language) }}</td>
                        </tr>
                        <tr>
                            <th>Subyek</th>
                            <td>{{ $collection->subject }}</td>
                        </tr>
                        <tr>
                            <th>Kategori/Kode</th>
                            <td>{{ $collection->collection_type->name }}/{{ strtoupper($collection->collection_type->code) }}</td>
                        </tr>

                        <tr>
                            <th>Deskripsi fisik</th>
                            <td>{{ $collection->desc }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar item
                    </h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Barcode</th>
                            <th>Status</th>
                            <th>Media</th>
                            <th>Lokasi</th>
                            <th>No. Rak</th>
                            <th>File</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($collection->items as $item)
                            <tr>
                                <td>{{ $item->code }}</td>
                                <td>{{ $item->item_status->status }}</td>
                                <td>{{ $item->media_type->name }}</td>
                                <td>{{ $item->location->name }}</td>
                                <td>{{ $item->no_rak }}</td>
                                <td>
                                    @if($item->file != null)
                                        <a href="{{ route('download',['file_id'=>$item->file->id]) }}">{{ $item->file->filename }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="6">Tidak ada item</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop