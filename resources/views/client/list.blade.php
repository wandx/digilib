@extends('client.master')

@section('contents')
    @include('client.partial.navbar')
    <div class="container">
        <div class="col-md-3 col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Filter</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('lists') }}">
                    <div class="form-group">
                        <label for="title">Kata kunci</label>
                        {!! Form::text('title',Request::input('title'),['class'=>'form-control','id'=>'title']) !!}
                    </div>
                    <div class="form-group">
                        <label for="title">ISSN/ISBN</label>
                        {!! Form::text('issn',Request::input('issn'),['class'=>'form-control','id'=>'issn']) !!}
                    </div>
                    <div class="form-group">
                        <label for="title">Penulis</label>
                        {!! Form::text('author',Request::input('author'),['class'=>'form-control','id'=>'author']) !!}
                    </div>
                    <div class="form-group">
                        <label for="title">Subyek</label>
                        {!! Form::text('subject',Request::input('subject'),['class'=>'form-control','id'=>'subject']) !!}
                    </div>
                    <div class="form-group">
                        <label for="">Media</label>
                        {!! Form::select('media',[''=>'Pilih Media']+getMedia(),null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="">Tipe Koleksi</label>
                        {!! Form::select('collection',[''=>'Pilih Tipe Koleksi']+getCollectionType(),null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="">Lokasi</label>
                        {!! Form::select('location',[''=>'Pilih Lokasi']+getLocation(),null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Terapkan filter</button>
                        <button type="button" class="btn btn-default btn-block" onclick="window.location='{{ route('lists') }}'">Reset</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-8" >
            @forelse($lists as $list)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <div class="pull-right">
                                    <b>Pengarang:</b> {{ $list->authors }} <br>
                                    <b>Ketersediaan:</b> {{ $list->available_item }} item
                                </div>
                                <div class="">
                                    <span class="title">{{ $list->title }}</span> <br><small>{{ $list->subtitle ?? '' }}</small>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('detail',['id'=>$list->id]) }}" class="btn btn-warning">Lihat detail</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="well text-center">
                    Pencarian tidak ditemukan.
                </div>
            @endforelse
            <div class="text-center">
                {{ $lists->render() }}
            </div>
        </div>
    </div>
@stop

@section('styles')
    <link rel="stylesheet" href="/wandx/css/typeahead.css">
@stop

@section('scripts')
    <script src="/wandx/js/typeahead.js"></script>
@stop