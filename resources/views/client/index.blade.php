@extends('client.master')

@section('contents')
    @include('client.partial.navbar')
    <div class="wrapper">
        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Perpustakaan</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('lists') }}">
                    <div id="searchbar">
                        <div class="input-group input-group-lg">
                            <input placeholder="Kata Kunci" name="title" id="title" type="text" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="advanced">
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="issn" id="issn" placeholder="ISSN/ISBN" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="subject" id="subject" placeholder="Subyek" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                {!! Form::select('media',[''=>'Pilih Media']+getMedia(),null,['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="author" id="author" placeholder="Pengarang" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                {!! Form::select('collection',[''=>'Pilih Tipe Koleksi']+getCollectionType(),null,['class'=>'form-control']) !!}
                            </div>
                            <div class="col-sm-4">
                                {!! Form::select('location',[''=>'Pilih Lokasi']+getLocation(),null,['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <span class="trigger-advanced bg-primary">Filter</span>
                    </form>
                </div>
            </div>

        </div>
    </div>


@stop

@section('styles')
    <link rel="stylesheet" href="/wandx/css/typeahead.css">
@stop

@section('scripts')
    <script src="/wandx/js/typeahead.js"></script>
@stop