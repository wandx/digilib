@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Siswa</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Siswa
        <small>Daftar siswa yang menjadi anggota perpustakaan</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table table-bordered table-hover" id="user-table">
                <thead>
                <tr>
                    <th rowspan="2">NIS</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">Alamat</th>
                    <th colspan="5">Status pinjaman</th>
                    <th rowspan="2">Aksi</th>
                </tr>
                <tr>
                    <th>Aktif</th>
                    <th>Terlambat</th>
                    <th>Selesai</th>
                    <th>Rusak</th>
                    <th>Hilang</th>
                </tr>

                </thead>
                <tbody>
                    @forelse($lists as $list)
                        <tr>
                            <td>{{ $list->nim }}</td>
                            <td>{{ $list->name }}</td>
                            <td>{{ $list->address }}</td>
                            <td class="text-center">{{ $list->active_loan }}</td>
                            <td class="text-center">{{ $list->late_loan }}</td>
                            <td class="text-center">{{ $list->done_loan }}</td>
                            <td class="text-center">{{ $list->broken_loan }}</td>
                            <td class="text-center">{{ $list->lost_loan }}</td>
                            <td class="text-center">
                                <a href="{{ route('adm.siswa.edit',['id'=>$list->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('adm.siswa.delete',['id'=>$list->id]) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                    @endforelse
                </tbody>
            </table>
            <div class="text-center">
                {{ $lists->render() }}
            </div>
        </div>
    </div>
@stop

@section('modals')

@stop

@section('scripts')

@stop

@section('styles')
    <style>
        table.table#user-table tr th{
            vertical-align: middle;
            text-align: center;
        }
    </style>
    
@stop