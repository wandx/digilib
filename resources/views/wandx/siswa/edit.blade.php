@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Siswa</a></li>
        <li><a href="#">Edit</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Edit Siswa
        <small>Edit siswa</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            {!! Form::model($user,["class"=>"form-horizontal"]) !!}
            <div class="form-group">
                <label for="nis" class="col-sm-3 control-label">NIS</label>
                <div class="col-sm-7">
                    <input value="{{ $user->nim }}" required type="text" class="form-control" id="nis" name="nim">
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-7">
                    <input  value="{{ $user->name }}" required type="text" class="form-control" id="name" name="name">
                </div>
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Address</label>
                <div class="col-sm-7">
                    <input value="{{ $user->address }}" required type="text" class="form-control" id="address" name="address">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-7">
                    <input value="{{ $user->email }}" required type="text" class="form-control" id="email" name="email">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 text-right">
                    <button class="btn btn-primary">Save</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('modals')

@stop

@section('scripts')

@stop