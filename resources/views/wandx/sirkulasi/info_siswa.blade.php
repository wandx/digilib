<table class="table">
    <tbody>
    <tr>
        <th>Nis</th>
        <td>:</td>
        <td>{{ $user->nim }}</td>
    </tr>
    <tr>
        <th>Nama</th>
        <td>:</td>
        <td>{{ $user->name }}</td>
    </tr>
    </tbody>
</table>
<table class="table list-pinjaman">
    <thead>
    <tr>
        <th colspan="5">Pinjaman aktif :</th>
    </tr>
    <tr>
        <th>Barcode</th>
        <th>ISSN/ISBN</th>
        <th>Judul</th>
        <th>Tgl. Pinjam</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @forelse($user->loans as $loan)

        <tr>
            <td>{{ $loan->code }}</td>
            <td>{{ $loan->collection->code }}</td>
            <td>{{ $loan->collection->title }}</td>
            <td>{{ \Carbon\Carbon::parse($loan->pivot->loan_start)->format('d M Y') }}</td>
            <td>
                @if(cekTerlambat($loan->pivot->id))
                    Terlambat
                @else
                    {{ getLoanStatus($loan->pivot->loan_status_id) }}
                @endif
            </td>
        </tr>
        @empty
            <tr>
                <td class="text-center" colspan="5">Tidak ada pinjaman</td>
            </tr>
    @endforelse

    </tbody>
</table>

