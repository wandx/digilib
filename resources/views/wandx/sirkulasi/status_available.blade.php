<div class="alert alert-success">{{ ucfirst($item->item_status->status) }} untuk dipinjamkan</div>
{!! Form::open(['class'=>'form-horizontal','route'=>'adm.sirkulasi.pinjamkan']) !!}
<div class="form-group">
    <label for="nis" class="control-label col-sm-3">NIS</label>
    <div class="col-sm-7">
        <input type="text" id="nis" name="nis" class="form-control">
    </div>

</div>

<div class="container-siswa">

</div>
<div class="form-group">
    <label for="in" class="control-label col-sm-3">Tgl. Pinjam</label>
    <div class="col-sm-7">
        <input type="text" name="in" class="form-control" id="in">
    </div>
</div>
<div class="form-group">
    <label for="ret" class="control-label col-sm-3">Tgl. Kembali</label>
    <div class="col-sm-7">
        <input type="text" name="ret" class="form-control" id="ret">
    </div>
</div>
<input type="hidden" name="item-id" value="{{ $item->id }}">
<div class="form-group">
    <div class="col-sm-10 text-right">
        <button type="submit" id="submit-me" disabled class="btn btn-primary">Lakukan Pinjaman</button>
    </div>
</div>
{!! Form::close() !!}