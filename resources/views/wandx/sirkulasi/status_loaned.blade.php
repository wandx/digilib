@php($loan = $item->loans->first())

    <table class="table">
        <tr>
            <th>Nama</th>
            <td>:</td>
            <td>{{ $loan->name }}</td>
        </tr>
        <tr>
            <th>Status</th>
            <td>:</td>
            <td>{{ cekTerlambat($loan->pivot->id) ? 'Terlambat':$item->item_status->status }}</td>
        </tr>
        <tr>
            <th>NIS</th>
            <td>:</td>
            <td>{{ $loan->nim }}</td>
        </tr>
        <tr>
            <th>Tgl. Pinjam</th>
            <td>:</td>
            <td>{{ \Carbon\Carbon::parse($loan->pivot->loan_start)->format('d M Y') }}</td>
        </tr>
        <tr>
            <th>Tgl. Kembali</th>
            <td>:</td>
            <td>{{ \Carbon\Carbon::parse($loan->pivot->loan_end)->format('d M Y') }}</td>
        </tr>
    </table>


<div class="text-center">
    {!! Form::open(['onsubmit'=>'return confirm("Apakah anda yakin?")','route'=>'adm.sirkulasi.kembalikan']) !!}
    <input type="hidden" name="loan-id" value="{{ $loan->pivot->id }}">
    <input type="hidden" name="item-id" value="{{ $item->id }}">
    <button class="btn btn-success">Lakukan Pengembalian</button>
    {!! Form::close() !!}

</div>