@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('adm.sirkulasi') }}">Sirkulasi</a></li>
        <li><a href="#">{{ $item->code }}</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        {{ $item->code }}
    </h1>
@stop

@section('contents')
    @if(session()->has('failed'))
    <div class="alert alert-danger">
        {{ session('failed') }}
    </div>
    @endif
    <div class="row">
        <div class="col-sm-6">
            @include('wandx.sirkulasi.informasi')
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Status</h3>
                </div>
                <div class="panel-body">
                    @if($item->item_status->code == "available")
                        @include('wandx.sirkulasi.status_available')
                    @elseif($item->item_status->code == "keep_by_staff")
                        @include('wandx.sirkulasi.keep_by_staff')
                    @elseif($item->item_status->code == "not_for_loan")

                    @else
                        @include('wandx.sirkulasi.status_loaned')
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop

@section('styles')
    <link rel="stylesheet" href="/wandx/css/typeahead.css">
    <link rel="stylesheet" href="/css/bd.css">
    <style>
        table th{
            text-align: right;
        }
        
        table.list-pinjaman th{
            text-align: left;
        }
    </style>
@stop

@section('scripts')
    <script src="/wandx/js/typeahead.js"></script>
    <script src="/js/momen.js"></script>
    <script src="/js/bd.js"></script>
    <script>
        $(function(){
            $('#in,#ret').datetimepicker({
                format:"YYYY-MM-DD"
            });
        });

        // typeahead for title
        $('#nis').typeahead({
                hint: true,
                highlight: true,
                minLength: 3
            },
            {
                name: 'states',
                source: function(query,sy,asy){
                    return $.getJSON('{{ route('adm.th-cir.nis') }}', { query: query }, function (data) {
                        return asy(data);
                    });
                }
        }).bind('typeahead:select',function(e,suggestion){
            var nis = $('#nis').val();
            if(nis === ""){
                alert('Nis Kosong');
                return false;
            }else{
                $.get('{{ route('adm.sirkulasi.siswa') }}/'+nis,function(data){
                    $('.container-siswa').html(data);
                })
            }
            $('#submit-me').prop('disabled',false);
        }).bind('typeahead:change',function(e,suggestion){
            $('#submit-me').prop('disabled',false);
        });
    </script>
@stop