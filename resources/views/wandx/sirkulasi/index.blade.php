@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Sirkulasi</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Sirkulasi
        <small>Daftar koleksi inventaris</small>
    </h1>
@stop

@section('contents')
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title">Filter</h3>
            {{--<div class="box-tools pull-right">--}}
            {{--<button class="btn btn-box-tool" type="button"><i class="fa fa-plus"></i></button>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <form action="" method="get" id="ffilter">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="code">Barcode</label>
                        <input type="text" name="code" class="form-control" id="code" value="{{ Request::input('code') }}">
                    </div>
                    <div class="col-sm-2">
                        <label for="issn">ISSN/ISBN</label>
                        <input type="text" name="issn" class="form-control" id="issn" value="{{ Request::input('issn') }}">
                    </div>
                    <div class="col-sm-2">
                        <label for="judul">Judul</label>
                        <input type="text" name="judul" class="form-control" id="judul" value="{{ Request::input('judul') }}">
                    </div>
                    <div class="col-sm-2">
                        <label for="pub">Penerbit</label>
                        <input type="text" name="penerbit" class="form-control" id="pub" value="{{ Request::input('penerbit') }}">
                    </div>
                    <div class="col-sm-2">
                        <label for="subject">Lokasi</label>
                        {!! Form::select('lokasi',[''=>'Semua Lokasi']+$lokasi,Request::input('lokasi'),['class'=>'form-control','onchange'=>'$("#ffilter").submit()']) !!}
                    </div>
                    <div class="col-sm-2">
                        <label for="subject">Status</label>
                        {!! Form::select('status',[''=>'Semua Status']+$status,Request::input('status'),['class'=>'form-control','onchange'=>'$("#ffilter").submit()']) !!}
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" onclick="window.location='{{ route('adm.sirkulasi') }}'"><i class="fa fa-refresh"></i> Reset</button>
                        <button class="btn btn-primary"><i class="fa fa-search"></i> Filter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <table class="table" id="collection-tb">
                <thead>
                <tr>
                    <th>Barcode</th>
                    <th>Judul</th>
                    <th>ISSN/ISBN</th>
                    <th>Penerbit</th>
                    <th>Lokasi</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
                </thead>

                <tbody>

                @forelse($lists as $list)
                    <tr>
                        <td>{{ $list->code }}</td>
                        <td>
                            {{ $list->collection->title }} {!! $list->collection->subtitle != null ? ' : <br>':null !!}
                            @if($list->collection->subtitle != null)
                                <small>{{ $list->collection->subtitle }}</small>
                            @endif
                        </td>
                        <td>{{ $list->collection->code }}</td>
                        <td>{{ $list->collection->pub }} ({{ ucfirst($list->collection->pub_city) }}, {{ $list->collection->pub_year }})</td>
                        <td>{{ $list->location->name }} ({{ $list->no_rak }})</td>
                        <td>{{ $list->item_status->status }}</td>
                        <td>
                            <a href="{{ route('adm.sirkulasi.detail',['code'=>$list->id]) }}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="7">Tidak ada data.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="box-footer text-center">
            {{ $lists->render() }}
        </div>
    </div>
@stop

@section('styles')
    <link rel="stylesheet" href="/wandx/css/typeahead.css">
@stop

@section('scripts')
    <script src="/wandx/js/typeahead.js"></script>
    <script>
        // typeahead for title
        $('#judul').typeahead({
                hint: true,
                highlight: true,
                minLength: 3
            },
            {
                name: 'states',
                source: function(query,sy,asy){
                    return $.getJSON('{{ route('adm.th-cir.title') }}', { query: query }, function (data) {
                        return asy(data);
                    });
                }
            }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });

        // typeahead for issn
        $('#issn').typeahead({
                hint: true,
                highlight: true,
                minLength: 4
            },
            {
                name: 'states',
                source: function(query,sy,asy){
                    return $.getJSON('{{ route('adm.th-cir.issn') }}', { query: query }, function (data) {
                        return asy(data);
                    });
                }
            }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });

        // typeahead for issn
        $('#code').typeahead({
                hint: true,
                highlight: true,
                minLength: 4
            },
            {
                name: 'states',
                source: function(query,sy,asy){
                    return $.getJSON('{{ route('adm.th-cir.code') }}', { query: query }, function (data) {
                        return asy(data);
                    });
                }
            }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });

        // typeahead for publisher
        $('#pub').typeahead({
                hint: true,
                highlight: true,
                minLength: 4
            },
            {
                name: 'states',
                source: function(query,sy,asy){
                    return $.getJSON('{{ route('adm.th-cir.pub') }}', { query: query }, function (data) {
                        return asy(data);
                    });
                }
            }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });
    </script>
@stop