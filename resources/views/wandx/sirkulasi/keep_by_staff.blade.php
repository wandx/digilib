<div class="alert alert-warning">
    Status item disimpan oleh staff, segera taruh ke rak dan klik tombol di bawah untuk konfirmasi.
</div>

<div class="text-center">
    {!! Form::open(['onsubmit'=>'return confirm("Apakah anda yakin?")','route'=>'adm.sirkulasi.kerak']) !!}
    <input type="hidden" name="item-id" value="{{ $item->id }}">
    <button type="submit" class="btn btn-primary">Item sudah dikembalikan.</button>
    {!! Form::close() !!}

</div>