<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Informasi</h3>
    </div>
    <div class="panel-body">
        <table class="table">
            <tbody>
            <tr>
                <th>{{ strtoupper($item->collection->type) }}</th>
                <td>{{ $item->collection->code }}</td>
            </tr>
            <tr>
                <th>Judul</th>
                <td>
                    {{ $item->collection->title }} {!! $item->collection->subtitle != null ? ' : <br>':null !!}
                    @if($item->collection->subtitle != null)
                        <small>{{ $item->collection->subtitle }}</small>
                    @endif
                </td>
            </tr>
            <tr>
                <th>Penerbit</th>
                <td>{{ $item->collection->pub }}, {{ $item->collection->pub_year }}</td>
            </tr>
            <tr>
                <th>Kota</th>
                <td>{{ ucfirst($item->collection->pub_city) }}</td>
            </tr>
            <tr>
                <th>Bahasa</th>
                <td>{{ ucfirst($item->collection->language) }}</td>
            </tr>
            <tr>
                <th>Subyek</th>
                <td>{{ $item->collection->subject }}</td>
            </tr>
            <tr>
                <th>Tipe Media</th>
                <td>{{ $item->media_type->name }}</td>
            </tr>
            <tr>
                <th>Kategori/Kode</th>
                <td>{{ $item->collection->collection_type->name }}/{{ strtoupper($item->collection->collection_type->code) }}</td>
            </tr>
            <tr>
                <th>Lokasi</th>
                <td>
                    {{ $item->location->name }},<br>
                    {{ $item->location->address }}
                </td>
            </tr>
            <tr>
                <th>No. Rak</th>
                <td>
                    {{ $item->no_rak }}
                </td>
            </tr>
            <tr>
                <th>Deskripsi fisik</th>
                <td>{{ $item->collection->desc }}</td>
            </tr>
            <tr>
                <th>Tgl input</th>
                <td>{{ $item->created_at->format('d M Y - H:i:s') }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>