@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Usermanager</a></li>
        <li class="active">Lists</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Usermanager
        <small>List of users</small>
    </h1>
@stop

@section('contents')
    @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Success</h4>
            {{session('success')}}
        </div>
    @endif
    <div class="box">
        {{--<div class="box-header with-border">--}}
            {{--<h3 class="box-title">User Lists</h3>--}}
        {{--</div>--}}
        <div class="box-body">
            <table class="table table-hover user-list">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Name</th>
                        <th>Created</th>
                        <th>Email</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($lists as $list)
                        <tr>
                            <td>
                                <img src="{{asset($list->avatar_img)}}" alt="" class="img50">
                                <a href="#" class="um-username-link">{{$list->username}}</a>
                                <span class="um-level">{{implode(', ',$list->roles->pluck('name')->toArray())}}</span>
                            </td>
                            <td>{{$list->name}}</td>
                            <td>{{$list->created_at->format('d/m/Y')}}</td>
                            <td>{{$list->email}}</td>
                            <td>

                                <span data-toggle="tooltip" title="Edit">
                                    <button data-id="{{$list->id}}" data-name="{{$list->name}}" class="btn btn-info btn-xs" data-toggle="modal" data-target="#edit-user"><i class="fa fa-cog"></i></button>
                                </span>
                                <span data-toggle="tooltip" title="Delete">
                                    <button onclick="deleteUser({{$list->id}})" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></button>
                                </span>

                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="5">No Data</td>
                            </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
    @if($lists->hasMorePages() || $lists->lastPage())
        <div class="text-center">
            {{$lists->render()}}
        </div>
    @endif

    <div class="btn-float">
        <button class="btn btn-primary" data-toggle="modal" data-target="#add-user">
            <i class="fa fa-plus"></i>
        </button>
    </div>

@stop

@section('modals')
    <!-- Modal -->
    <div id="add-user" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                {!! Form::open(['route'=>'adm.usermanager.store','id'=>'add-user-form','files'=>true]) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <div class="photo-selector" onclick="$('#avatar').trigger('click')">
                            <div class="img-holder">
                                <span class="img-text-holder">Pilih foto</span>
                                <img src="" alt="" class="hidden" id="img-prev">
                            </div>
                        </div>
                        <input type="file" name="avatar" class="hidden" id="avatar" onchange="readFile(this,'#img-prev')">
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input required type="text" name="name" id="name" class="form-control" placeholder="Nama lengkap user">
                    </div>

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input required type="text" name="username" id="username" class="form-control" placeholder="Username">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input required type="email" name="email" id="email" class="form-control" placeholder="Email user">
                    </div>

                    <div class="form-group has-feedback">
                        <label for="password">Passowod</label>
                        <input required type="password" name="password" class="form-control" id="password" placeholder="Password user">
                        <span onclick="showPassword('#password')" class="fa fa-eye form-control-feedback clickable"></span>
                    </div>

                    <div class="form-group">
                        <label for="role">Role</label>
                        {!! Form::select('roles[]',roleData(),null,['class'=>'form-control','multiple'=>'true','required']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="edit-user" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                {!! Form::open(['id'=>'edit-user-form','files'=>true]) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <div class="photo-selector" onclick="$('#avatar2').trigger('click')">
                            <div class="img-holder">
                                <span class="img-text-holder">Pilih foto</span>
                                <img src="" alt="" class="hidden" id="img-prev2">
                            </div>
                        </div>
                        <input type="file" name="avatar" class="hidden" id="avatar2" onchange="readFile(this,'img#img-prev2')">
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input required type="text" name="name" id="name" class="form-control" placeholder="Nama lengkap user">
                    </div>

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input required type="text" name="username" id="username" class="form-control" placeholder="Username">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input required type="email" name="email" id="email" class="form-control" placeholder="Email user">
                    </div>

                    <div class="form-group has-feedback">
                        <label for="password">Passowod</label>
                        <input type="password" name="password" class="form-control" id="password2" placeholder="type new password if want to change">
                        <span onclick="showPassword('#password2')" class="fa fa-eye form-control-feedback clickable"></span>
                    </div>

                    <div class="form-group">
                        <label for="role">Role</label>
                        {!! Form::select('roles[]',roleData(),null,['class'=>'form-control','multiple'=>'true','required','id'=>'roles']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@stop

@section('scripts')
    <script src="/vendors/validator/jquery.validate.min.js"></script>
    <script src="/wandx/js/usermanager/lists.js"></script>
@stop