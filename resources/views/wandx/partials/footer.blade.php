<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Digital Library
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017<a href="#">WandyPurnomo</a>.</strong> All rights reserved.
</footer>