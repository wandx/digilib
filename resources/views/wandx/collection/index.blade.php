@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Koleksi</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Koleksi
        <small>Daftar koleksi inventaris</small>
    </h1>
@stop

@section('contents')
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title">Filter</h3>
            {{--<div class="box-tools pull-right">--}}
                {{--<button class="btn btn-box-tool" type="button"><i class="fa fa-plus"></i></button>--}}
            {{--</div>--}}
        </div>
        <div class="box-body">
            <form action="" method="get" id="ffilter">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="issn">ISSN/ISBN</label>
                        <input type="text" name="code" class="form-control" id="issn" value="{{ Request::input('code') }}">
                    </div>
                    <div class="col-sm-3">
                        <label for="judul">Judul</label>
                        <input type="text" name="judul" class="form-control" id="judul" value="{{ Request::input('judul') }}">
                    </div>
                    <div class="col-sm-3">
                        <label for="pub">Penerbit</label>
                        <input type="text" name="penerbit" class="form-control" id="pub" value="{{ Request::input('penerbit') }}">
                    </div>
                    <div class="col-sm-3">
                        <label for="subject">Subyek</label>
                        <input type="text" name="subyek" class="form-control" id="subject" value="{{ Request::input('subyek') }}">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" onclick="window.location='{{ route('adm.collection') }}'"><i class="fa fa-refresh"></i> Reset</button>
                        <button class="btn btn-primary"><i class="fa fa-search"></i> Filter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <table class="table" id="collection-tb">
                <thead>
                    <tr>
                        <th>ISSN/ISBN</th>
                        <th>Judul</th>
                        <th>Jml.Item</th>
                        <th>Tersedia</th>
                        <th>Dipinjam</th>
                        <th>onStaff</th>
                        <th>Penerbit</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody>

                    @forelse($lists as $list)
                        <tr>
                            <td>{{ $list->code }}</td>
                            <td>
                                {{ $list->title }} {!! $list->subtitle != null ? ' : <br>':null !!}
                                @if($list->subtitle != null)
                                    <small>{{ $list->subtitle }}</small>
                                @endif
                            </td>
                            <td>{{ $list->jml_item }}</td>
                            <td>{{ $list->available_item }}</td>
                            <td>{{ $list->loaned_item }}</td>
                            <td>{{ $list->on_staff }}</td>
                            <td>{{ $list->pub }} ({{ ucfirst($list->pub_city) }}, {{ $list->pub_year }})</td>
                            <td>
                                <a href="{{ route('adm.collection.edit',['id'=>$list->id]) }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('adm.collection.destroy',['id'=>$list->id]) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="7">Tidak ada data.</td>
                            </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="box-footer text-center">
            {{ $lists->render() }}
        </div>
    </div>
@stop

@section('styles')
    <link rel="stylesheet" href="/wandx/css/typeahead.css">
@stop

@section('scripts')
    <script src="/wandx/js/typeahead.js"></script>
    <script>
        // typeahead for title
        $('#judul').typeahead({
            hint: true,
            highlight: true,
            minLength: 3
        },
        {
            name: 'states',
            source: function(query,sy,asy){
                return $.getJSON('{{ route('adm.th.title') }}', { query: query }, function (data) {
                    return asy(data);
                });
            }
        }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });

        // typeahead for issn
        $('#issn').typeahead({
            hint: true,
            highlight: true,
            minLength: 4
        },
        {
            name: 'states',
            source: function(query,sy,asy){
                return $.getJSON('{{ route('adm.th.issn') }}', { query: query }, function (data) {
                    return asy(data);
                });
            }
        }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });

        // typeahead for publisher
        $('#pub').typeahead({
                hint: true,
                highlight: true,
                minLength: 4
            },
            {
                name: 'states',
                source: function(query,sy,asy){
                    return $.getJSON('{{ route('adm.th.pub') }}', { query: query }, function (data) {
                        return asy(data);
                    });
                }
            }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });

        //typeahead for subject
        $('#subject').typeahead({
                hint: true,
                highlight: true,
                minLength: 4
            },
            {
                name: 'states',
                source: function(query,sy,asy){
                    return $.getJSON('{{ route('adm.th.subject') }}', { query: query }, function (data) {
                        return asy(data);
                    });
                }
            }).bind('typeahead:select',function(e,suggestion){
            $('#ffilter').submit();
        });
    </script>
@stop