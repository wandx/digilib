@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Collection</a></li>
        <li><a href="#">Add</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Add Collection
        <small>Add collection</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <div class="col-sm-8 col-sm-offset-2">


            {!! Form::open() !!}
            <div class="form-group">
                <label for="tipe">Tipe kode</label>
                {!! Form::select('type',['issn'=>'ISSN','isbn'=>'ISBN'],null,['class'=>'form-control','id'=>'tipe']) !!}
            </div>

            <div class="form-group">
                <label for="code">Kode</label>
                {!! Form::text('code',null,['class'=>'form-control','id'=>'code']) !!}
            </div>

            <div class="form-group">
                <label for="title">Judul</label>
                {!! Form::text('title',null,['class'=>'form-control','id'=>'title']) !!}
            </div>

            <div class="form-group">
                <label for="subtitle">Sub judul</label>
                {!! Form::text('subtitle',null,['class'=>'form-control','id'=>'subtitle']) !!}
            </div>

            <div class="form-group">
                <label for="lang">Bahasa</label>
                {!! Form::text('language',null,['class'=>'form-control','id'=>'lang']) !!}
            </div>

            <div class="form-group">
                <label for="subject">Subyek</label>
                {!! Form::text('subject',null,['class'=>'form-control','id'=>'subject']) !!}
            </div>

            <div class="form-group">
                <label for="pub">Penerbit</label>
                {!! Form::text('pub',null,['class'=>'form-control','id'=>'pub']) !!}
            </div>

            <div class="form-group">
                <label for="pub_year">Tahun terbit</label>
                {!! Form::text('pub_year',null,['class'=>'form-control','id'=>'pub_year']) !!}
            </div>

            <div class="form-group">
                <label for="pub_city">Kota terbit</label>
                {!! Form::text('pub_city',null,['class'=>'form-control','id'=>'pub_city']) !!}
            </div>

            <div class="form-group">
                <label for="authors">Pengarang</label>
                {!! Form::text('authors',null,['class'=>'form-control','id'=>'authors']) !!}
            </div>

            <div class="form-group">
                <label for="collection_type_id">Kategori</label>
                {!! Form::select('collection_type_id',$category,null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                <label for="desc">Description</label>
                {!! Form::text('desc',null,['class'=>'form-control','id'=>'desc']) !!}
            </div>



            <div class="form-group text-right">
                    <button class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('modals')

@stop

@section('scripts')

@stop