{!! Form::model($item,['files'=>true,'route'=>['adm.collection.item.update','item_id'=>$item->id]]) !!}
<div class="form-group">
    <label for="">Barcode</label>
    {!! Form::text('code',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
    <label for="">Status</label>
    {!! Form::select('status_id',$status,null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    <label for="">Lokasi</label>
    {!! Form::select('location_id',$location,null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
    <label for="">Nomer Rak</label>
    {!! Form::text('no_rak',null,['class'=>'form-control','required']) !!}
</div>
<div class="form-group">
    <label for="">Media</label>
    {!! Form::select('media_type_id',$media,null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    <label for="">File</label>
    {!! Form::file('file',['class'=>'form-control']) !!}
    <span class="help-block">Max 10Mb</span>
</div>
<div class="text-right">
    <button type="submit" class="btn btn-primary">Simpan</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

</div>
{!! Form::close() !!}