@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ route('adm.collection') }}">Koleksi</a></li>
        <li><a href="#">Edit</a></li>
        <li><a href="#">{{ $collection->title }}</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Edit Koleksi
        <small>{{ $collection->title }}</small>
    </h1>
@stop

@section('contents')
    <div class="row">
        <div class="col-sm-4">
            {!! Form::model($collection) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Informasi</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="tipe">Tipe kode</label>
                        {!! Form::select('type',['issn'=>'ISSN','isbn'=>'ISBN'],null,['class'=>'form-control','id'=>'tipe']) !!}
                    </div>

                    <div class="form-group">
                        <label for="code">Kode</label>
                        {!! Form::text('code',null,['class'=>'form-control','id'=>'code']) !!}
                    </div>

                    <div class="form-group">
                        <label for="title">Judul</label>
                        {!! Form::text('title',null,['class'=>'form-control','id'=>'title']) !!}
                    </div>

                    <div class="form-group">
                        <label for="subtitle">Sub judul</label>
                        {!! Form::text('subtitle',null,['class'=>'form-control','id'=>'subtitle']) !!}
                    </div>

                    <div class="form-group">
                        <label for="lang">Bahasa</label>
                        {!! Form::text('language',null,['class'=>'form-control','id'=>'lang']) !!}
                    </div>

                    <div class="form-group">
                        <label for="subject">Subyek</label>
                        {!! Form::text('subject',null,['class'=>'form-control','id'=>'subject']) !!}
                    </div>

                    <div class="form-group">
                        <label for="pub">Penerbit</label>
                        {!! Form::text('pub',null,['class'=>'form-control','id'=>'pub']) !!}
                    </div>

                    <div class="form-group">
                        <label for="pub_year">Tahun terbit</label>
                        {!! Form::text('pub_year',null,['class'=>'form-control','id'=>'pub_year']) !!}
                    </div>

                    <div class="form-group">
                        <label for="pub_city">Kota terbit</label>
                        {!! Form::text('pub_city',null,['class'=>'form-control','id'=>'pub_city']) !!}
                    </div>

                    <div class="form-group">
                        <label for="authors">Pengarang</label>
                        {!! Form::text('authors',null,['class'=>'form-control','id'=>'authors']) !!}
                    </div>

                    <div class="form-group">
                        <label for="collection_type_id">Kategori</label>
                        {!! Form::select('collection_type_id',$category,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="desc">Description</label>
                        {!! Form::text('desc',null,['class'=>'form-control','id'=>'desc']) !!}
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Items</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Barcode</th>
                            <th>Status</th>
                            <th>Media</th>
                            <th>Lokasi</th>
                            <th>No. Rak</th>
                            <th>File</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($collection->items as $item)
                            <tr>
                                <td>{{ $item->code }}</td>
                                <td>{{ $item->item_status->status }}</td>
                                <td>{{ $item->media_type->name }}</td>
                                <td>{{ $item->location->name }}</td>
                                <td>{{ $item->no_rak }}</td>
                                <td>{{ $item->file->filename ?? "-" }}</td>
                                <td>
                                    <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#item-modal" data-id="{{ $item->id }}"><i class="fa fa-pencil"></i></button>
                                    <a href="{{ route('adm.collection.delete_item',['id'=>$item->id]) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                    @if($item->file != null)

                                        <button onclick="deleteFile('{{ $item->file->id ?? "-" }}')" class="btn btn-danger btn-xs"><i class="fa fa-file"></i></button>
                                    @endif
                                </td>
                            </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="7">Tidak ada item</td>
                                </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="text-center">
                        <button data-toggle="modal" data-target="#item-add" class="btn btn-primary">Tambah Item</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section('modals')
    <!-- Modal -->
    <div id="item-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Item</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="item-add" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['files'=>true,'route'=>'adm.collection.item.store','item_id']) !!}
                    <input type="hidden" name="collection-id" value="{{ $collection->id }}">
                    <div class="form-group">
                        <label for="">Barcode</label>
                        {!! Form::text('code',null,['class'=>'form-control','required']) !!}
                    </div>

                    <div class="form-group">
                        <label for="">Status</label>
                        {!! Form::select('status_id',$status,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="">Lokasi</label>
                        {!! Form::select('location_id',$location,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="">Nomer Rak</label>
                        {!! Form::text('no_rak',null,['class'=>'form-control','required']) !!}
                    </div>


                    <div class="form-group">
                        <label for="">Media</label>
                        {!! Form::select('media_type_id',$media,null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label for="">File</label>
                        {!! Form::file('file',['class'=>'form-control']) !!}
                        <span class="help-block">Max 10Mb</span>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>

@stop

@section('scripts')
    <script>
        $('#item-modal').on('show.bs.modal',function(e){
            var trig = $(e.relatedTarget),
                modal = $(this),
                id = trig.data('id');

            $.get('{{ route('adm.collection.item') }}/'+id,function(data){
                modal.find('.modal-body').html(data);
            })
        })

        function deleteFile(id) {
            if(id === "-"){
                alert('Tidak ada file');
                return false;
            }
            var c = confirm('Hapus file?');
            if(c){
                $.get('{{ route('adm.collection.delete_file') }}/'+id,function(data){
                    location.reload();
                })
            }

        }
    </script>
@stop